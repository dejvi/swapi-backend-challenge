const mongoose = require('mongoose');

const StarshipSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    manufacturer: {
        type: String,
        required: true
    },
    cost_in_credits: {
        type: String,
        required: true
    },
    length: {
        type: String,
        required: true
    },
    max_atmosphering_speed: {
        type: String,
        required: true
    },
    crew: {
        type: String,
        required: true
    },
    passengers: {
        type: String,
        required: true
    },
    cargo_capacity: {
        type: String,
        required: true
    },
    consumables: {
        type: String,
        required: true
    },
    hyperdrive_rating: {
        type: String,
        required: true
    },
    MGLT: {
        type: String,
        required: true
    },
    starship_class: {
        type: String,
        required: true
    },
    pilots: {
        type: [String],
        required: true
    },
    films: {
        type: [String],
        required: true
    },
    created: {
        type: String,
        required: true
    },
    edited: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    count: {
        type: String,
        required: false
    },
});

module.exports = Starship = mongoose.model('starship', StarshipSchema);