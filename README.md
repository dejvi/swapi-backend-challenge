To set up:

Run 'npm install' to install the required packages

Create a new mongodb database and put the URI in default.json

Run the server with 'npm run server'

Make a POST request on '../api/starships/seed' and '../api/vehicles/seed' to get initial data from SWAPI

then you can get, set, increment and decrement the count field by name of starship or vehicle
