const express = require('express');
const router = express.Router();
const request = require('request');

const Vehicle = require('../../models/Vehicle');

router.post('/', async (req, res) => {
    const { name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed,
        crew, passengers, cargo_capacity, consumables,
        vehicle_class, pilots, films, created, edited, url, count } = req.body;

    try {

        vehicle = new Vehicle({
            name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed,
            crew, passengers, cargo_capacity, consumables,
            vehicle_class, pilots, films, created, edited, url, count
        });

        await vehicle.save();

        res.send('Vehicle added');
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error.');
    }
});


function requestPromise(options) {
    return new Promise(function(resolve, reject){
      request(options, (err, response, body) => {
        if (err) {
          reject(err);
          return;
        }
        try {
            resolve(JSON.parse(body));
         } catch(err) {
           reject(err); // JSON parse error
           return;
         }
      });
  })
}

// seeding the database with data from swapi
router.post('/seed', async (req, res) => {
    try {
        var pageNr = 1;
         while(true) {
            const options = {
                uri: `http://swapi.dev/api/vehicles/?page=${pageNr}`,
                method: 'GET',
                headers: { 'user-agent': 'node.js' }
            }

            const { next, results } = await requestPromise(options);

            var i = 0;
            while(i < results.length) {
                const { name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed,
                    crew, passengers, cargo_capacity, consumables,
                    vehicle_class, pilots, films, created, edited, url, count
                } = results[i];
                
    
                console.log(results[i]);
                i++;
                
                vehicle = new Vehicle({
                    name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed,
                    crew, passengers, cargo_capacity, consumables,
                    vehicle_class, pilots, films, created, edited, url, count
                });
    
                await vehicle.save();
            }
                
            if (next == null){
                break;
            }
            else {
                pageNr++;
            }
        }
        res.send(`Added ${pageNr} pages of vehicles from SWAPI`);    
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error.');
    }
})

// Get the count of a vehicle by its name
router.get('/count/:vehicle_name', async (req, res) => {
    try {
        const vehicle = await Vehicle.findOne({ name: req.params.vehicle_name });

        if(!vehicle) return res.status(400).json({ msg: 'There is no vehicle with this name'});

        console.log(vehicle.count);

        res.json({ "count" : vehicle.count });
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error.');
    }
}) 

// Set the count of a vehicle by its name
router.put('/count/:vehicle_name', async (req, res) => {
    try {

        if(!req.body.count) return res.status(400).json({ msg: 'Please add the count'});

        const vehicle = await Vehicle.findOne({ name: req.params.vehicle_name });

        if(!vehicle) return res.status(400).json({ msg: 'There is no vehicle with this name'});

        vehicle.count = req.body.count;

        await vehicle.save();

        res.json(vehicle); 
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error.');
    }
})

// Increment the count of a vehicle by its name
router.put('/increment/:vehicle_name/:unit', async (req, res) => {
    try {

        const vehicle = await Vehicle.findOne({ name: req.params.vehicle_name });

        if(!vehicle) return res.status(400).json({ msg: 'There is no vehicle with this name'});

        vehicle.count = Number(vehicle.count) + Number(req.params.unit);

        await vehicle.save();

        res.json(vehicle); 
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error.');
    }
})

// Decrement the count of a vehicle by its name
router.put('/decrement/:vehicle_name/:unit', async (req, res) => {
    try {

        const vehicle = await Vehicle.findOne({ name: req.params.vehicle_name });

        if(!vehicle) return res.status(400).json({ msg: 'There is no vehicle with this name'});

        vehicle.count = Number(vehicle.count) - Number(req.params.unit);

        await vehicle.save();

        res.json(vehicle); 
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error.');
    }
})

module.exports = router;
