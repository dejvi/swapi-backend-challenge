const express = require('express');
const router = express.Router();
const request = require('request');

const Starship = require('../../models/Starship');

router.post('/', async (req, res) => {
    const { name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed,
        crew, passengers, cargo_capacity, consumables, hyperdrive_rating, MGLT,
        starship_class, pilots, films, created, edited, url, count } = req.body;

    try {

        starship = new Starship({
            name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed,
            crew, passengers, cargo_capacity, consumables, hyperdrive_rating, MGLT,
            starship_class, pilots, films, created, edited, url, count
        });

        await starship.save();

        res.send('Starship added');
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error.');
    }
});


function requestPromise(options) {
    return new Promise(function(resolve, reject){
      request(options, (err, response, body) => {
        if (err) {
          reject(err);
          return;
        }
        try {
            resolve(JSON.parse(body));
         } catch(err) {
           reject(err); // JSON parse error
           return;
         }
      });
  })
}

// seeding the database with data from swapi
router.post('/seed', async (req, res) => {
    try {
        var pageNr = 1;
         while(true) {
            const options = {
                uri: `http://swapi.dev/api/starships/?page=${pageNr}`,
                method: 'GET',
                headers: { 'user-agent': 'node.js' }
            }

            const { next, results } = await requestPromise(options);

            var i = 0;
            while(i < results.length) {
                const { name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed,
                    crew, passengers, cargo_capacity, consumables, hyperdrive_rating, MGLT,
                    starship_class, pilots, films, created, edited, url, count } = results[i];
                
    
                console.log(results[i]);
                i++;
                
                starship = new Starship({
                    name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed,
                    crew, passengers, cargo_capacity, consumables, hyperdrive_rating, MGLT,
                    starship_class, pilots, films, created, edited, url, count
                });
    
                await starship.save();
            }
                
            if (next == null){
                break;
            }
            else {
                pageNr++;
            }
        }
        res.send(`Added ${pageNr} pages of starships from SWAPI`);    
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error.');
    }
})

// Get the count of a starship by its name
router.get('/count/:starship_name', async (req, res) => {
    try {
        const starship = await Starship.findOne({ name: req.params.starship_name });

        if(!starship) return res.status(400).json({ msg: 'There is no starship with this name'});

        console.log(starship.count);

        res.json({ "count" : starship.count });
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error.');
    }
}) 

// Set the count of a starship by its name
router.put('/count/:starship_name', async (req, res) => {
    try {

        if(!req.body.count) return res.status(400).json({ msg: 'Please add the count'});

        const starship = await Starship.findOne({ name: req.params.starship_name });

        if(!starship) return res.status(400).json({ msg: 'There is no starship with this name'});

        starship.count = req.body.count;

        await starship.save();

        res.json(starship); 
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error.');
    }
})

// Increment the count of a starship by its name
router.put('/increment/:starship_name/:unit', async (req, res) => {
    try {

        const starship = await Starship.findOne({ name: req.params.starship_name });

        if(!starship) return res.status(400).json({ msg: 'There is no starship with this name'});

        starship.count = Number(starship.count) + Number(req.params.unit);

        await starship.save();

        res.json(starship); 
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error.');
    }
})

// Decrement the count of a starship by its name
router.put('/decrement/:starship_name/:unit', async (req, res) => {
    try {

        const starship = await Starship.findOne({ name: req.params.starship_name });

        if(!starship) return res.status(400).json({ msg: 'There is no starship with this name'});

        starship.count = Number(starship.count) - Number(req.params.unit);

        await starship.save();

        res.json(starship); 
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error.');
    }
})

module.exports = router;
